FROM nginx:1.21.6-alpine

COPY ./default.conf /etc/nginx/conf.d/
COPY ./index.html /www/

EXPOSE 80 443

CMD ["nginx", "-g", "daemon off;"]